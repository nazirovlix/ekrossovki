<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $code
 * @property string $base_url
 * @property string $path
 * @property string $title_ru
 * @property string $title_uz
 * @property string $content_ru
 * @property string $content_uz
 * @property int $category_id
 * @property int $price
 * @property int $discount_price
 * @property int $discount
 * @property int $country_id
 * @property int $brand_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $status
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'category_id', 'price', 'discount_price', 'discount', 'country_id', 'brand_id', 'created_at', 'updated_at', 'status'], 'integer'],
            [['title_ru', 'title_uz', 'content_ru', 'content_uz', 'category_id'], 'required'],
            [['content_ru', 'content_uz'], 'string'],
            [['base_url', 'path', 'title_ru', 'title_uz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'base_url' => Yii::t('app', 'Base Url'),
            'path' => Yii::t('app', 'Path'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'title_uz' => Yii::t('app', 'Title Uz'),
            'content_ru' => Yii::t('app', 'Content Ru'),
            'content_uz' => Yii::t('app', 'Content Uz'),
            'category_id' => Yii::t('app', 'Category ID'),
            'price' => Yii::t('app', 'Price'),
            'discount_price' => Yii::t('app', 'Discount Price'),
            'discount' => Yii::t('app', 'Discount'),
            'country_id' => Yii::t('app', 'Country ID'),
            'brand_id' => Yii::t('app', 'Brand ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
