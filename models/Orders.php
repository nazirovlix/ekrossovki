<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $uesr_id
 * @property int $amount
 * @property int $state
 * @property string $delivery_time
 * @property int $created_at
 * @property int $updated_at
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uesr_id', 'amount'], 'required'],
            [['uesr_id', 'amount', 'state', 'created_at', 'updated_at'], 'integer'],
            [['delivery_time'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uesr_id' => Yii::t('app', 'Uesr ID'),
            'amount' => Yii::t('app', 'Amount'),
            'state' => Yii::t('app', 'State'),
            'delivery_time' => Yii::t('app', 'Delivery Time'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
