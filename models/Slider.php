<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property int $id
 * @property string $path
 * @property int $product_id
 * @property string $title_ru
 * @property string $title_uz
 * @property int $order
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'slider';
    }
    public function behaviors()
    {
        return [
            [
                'class' => '\app\components\FileUploadBehaviour'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'order'], 'integer'],
            [['title_ru', 'title_uz'], 'required'],
            [[ 'title_ru', 'title_uz'], 'string', 'max' => 255],
            [['path'],'file']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'path' => Yii::t('app', 'Image'),
            'product_id' => Yii::t('app', 'Product ID'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'title_uz' => Yii::t('app', 'Title Uz'),
            'order' => Yii::t('app', 'Order'),
        ];
    }

    /**
     * @return int
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
