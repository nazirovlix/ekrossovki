<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "carousel".
 *
 * @property int $id
 * @property string $base_url
 * @property string $path
 * @property string $product_id
 * @property string $title_ru
 * @property string $title_uz
 * @property string $content_ru
 * @property string $content_uz
 * @property string $price
 * @property int $order
 * @property int $created_at
 * @property int $updated_at
 */
class Carousel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'carousel';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => '\app\components\FileUploadBehaviour'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'title_ru', 'title_uz', 'content_ru', 'content_uz'], 'required'],
            [['content_ru', 'content_uz'], 'string'],
            [['order', 'created_at', 'updated_at'], 'integer'],
            [['product_id', 'title_ru', 'title_uz', 'price'], 'string', 'max' => 255],
            [['path',], 'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'path' => Yii::t('app', 'Image'),
            'product_id' => Yii::t('app', 'Product ID'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'title_uz' => Yii::t('app', 'Title Uz'),
            'content_ru' => Yii::t('app', 'Content Ru'),
            'content_uz' => Yii::t('app', 'Content Uz'),
            'price' => Yii::t('app', 'Price'),
            'order' => Yii::t('app', 'Order'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getProduct() {
      return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
